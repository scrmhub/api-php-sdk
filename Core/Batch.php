<?php
namespace SCRMHub\SDK\Core;

use SDK;

class Batch {
    private
        $calls = array('batch' => array());

    /**
     * Add a request
     */
    function add($api, $opts) {
        //Add the api as the endpoint
        $opts['endpoint'] = $api;

        //Add the call
        $this->calls['batch'][] = $opts;
    }

    /**
     * Call everything
     */
    function call() {
        //Build the request and response
        $request    = new Request();
        $response   = $request->call('batch', $this->calls);

        //Stop a double call
        $this->calls = null;

        //Return the response
        return $response;
    }
}