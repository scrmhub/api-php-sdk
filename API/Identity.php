<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;
use SCRMHub\SDK\API;

class Identity extends Base {
    /**
     * Our variables
     */
    protected
        $api = 'identity',
        $baseRequest = array(
            'action'        => null,
            'puuid'         => null
        );

    public function getApiUrl() {
        $apiCore = API::getInstance();

        $url  = $apiCore->getOptions()['options']['url'];
        $url .= $this->api . '/';

        return $url;
    }

    public function connect($data) {
        $apiCore = API::getInstance();
        $oldOptions = $apiCore->getOptions()['options']['curlOptions'];
        $newOptions = array(
            CURLOPT_MAXREDIRS => 1,
            // CURLOPT_FOLLOWLOCATION => 0
        );

        $apiCore->setup(array(
            'curlOptions' => $oldOptions + $newOptions
        ));

        $response = parent::connect($data);
        $apiCore->setup(array('curlOptions' => $oldOptions));

        return $response;
    }
}