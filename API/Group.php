<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Group extends Base {
        /**
         * Our variables
         */
        protected
                $api = 'group',
                $baseRequest = array(
                        'action'        => null,
                        'usertoken'     => null,
                        'id'            => null,
                        'ids'           => array(),
                        'name'          => null,
                        'text'          => null,
                        'mid'           => null,
                        'delete'        => false,
                        'invite'        => null,
                        'notification'  => null,
                        'new'           => null,
                        'from'          => null,
                        'to'            => null,
                        'page'          => null,
                        'count'         => null
                );
}