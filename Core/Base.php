<?php
namespace SCRMHub\SDK\Core;

use SCRMHub\SDK\API;
use SCRMHub\SDK\Core\Request;

class Base {
    private
        $data,
        $response,
        $methodConvert = array(
            'save' => 'store'
        );

    protected
        $api,
        $baseRequest,
        $baseHeaders;

    /**
     * Magic method
     *
     * Return response object
     */
    public function __call($method, $data) {
        //Is it a method name we need to remap?
        if(isset($this->methodConvert[$method])) {
            $method = $this->methodConvert[$method];
        }

        //Set the action
        $data[0]['action'] = $method;

        //return the object
        return $this->load($data[0]);
    }

    /**
     * Make an api load call
     *
     * Return true/false
     */
    public function load($data) {
        //Make the correct data object
        $data = $this->mergeRequest($data);

        //return the object
        return $this->call($data);
    }

    /**
     * Return the base request data
     * This can be overriden by the api class if required
     */
    protected function getBaseRequest($data) {
        return $this->baseRequest;
    }

    /**
     * Merge two data sets
     */
    protected function mergeRequest($data) {
        //Call the function
        $call = $this->getBaseRequest($data);

        //Merge the two
        return array_merge($call, $data);
    }

    /**
     * Search for an instance using a primary identifier
     *
     * Return true/false
     */
    public function call($requestData) {
        //Is it a batch
        if(API::isBatch()) {
            API::addToBatch($this->api, $requestData);
            return;
        }

        //Otherwise build a request
        $request = new Request();
        $request->setHeaders($this->getBaseHeaders());
        $response = $request->call($this->api, $requestData);

        return $response;
    }

    /**
     * Return the base request data
     * This can be overriden by the api class if required
     */
    protected function getBaseHeaders() {
        $this->baseHeaders['ip'] = $this->getRealIp();
        $this->baseHeaders['user-agent'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        $this->baseHeaders['referer'] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;

        //Return the object
        return $this->baseHeaders;
    }

    /**
     * Save an activity
     *
     * Return an id for the activity
     */
    private function getRealIp() {
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            //check for ip from share internet
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            // Check for the Proxy User
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            //The usual
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }
}

