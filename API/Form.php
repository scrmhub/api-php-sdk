<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Form extends Base {
        /**
         * Our variables
         */
        protected
                $api = 'form',
                $baseRequest = array(
                        'action'        => null,
                        'fuuid'         => null,
                        'puuid'         => null,
                        'data'          => null
                );
}