<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Friend extends Base {
        /**
         * Our variables
         */
        protected
                $api = 'friend',
                $baseRequest = array(
                        'action'        => null,
                        'usertoken'     => null,
                        'id'            => null
                );
}