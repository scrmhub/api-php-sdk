<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Url extends Base {
    /**
     * Our variables
     */
    protected
        $api = 'url',
        $baseRequest = array(
            'action'        => null, //create, get
            'url'           => null, //(optional): external url to link to, or, url of custom application deep link
            'token'         => null, //(optional): authenticated brand person token - creates the hash specifically for that user
            'activity_id'   => null, //(optional, legacy, probably deprecated in future): id of specific activity that caused this url hash to be created
            'hash'          => null, //(optional): custom hash, must be under 10 chars and unique. Giving a non unique hash will return the hash but nothing will be overwritten
            'outbound'      => null  //(optional): force the resolver to consider it an outbound url
        );
}