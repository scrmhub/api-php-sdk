<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Helper extends Base {
    /**
     * Our variables
     */
    protected
        $api = 'helper',
        $baseRequest = array(
            'action'        => null,
            'puuid'         => null
        );
}