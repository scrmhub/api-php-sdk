<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Auth extends Base {
        /**
         * Our variables
         */
        protected
                $api = 'auth',
                $baseRequest = array(
                        'action'        => null,
                        'id'            => null,
                        'pw'            => null,
                        'newpw'         => null,
                        'auth_token'    => null,
                        'email'         => null
                );
}