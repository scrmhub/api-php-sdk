<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Notification extends Base {
        /**
         * Our variables
         */
        protected
                $api = 'notification',
                $baseRequest = array(
                        'action'        => null,
                        'usertoken'     => null,
                        'ids'           => array(),
                        'date'          => null
                );
}