<?php

include_once "vendor/autoload.php";

use SCRMHub\SDK\API;

API::init(array(
    'appkey'   => 'd6b26d54-c84d-3814-dd3e-aecc5610d74a',
));

$api = new API\Person();
$data = array(
    'ids' => array(
        'facebook_id',
        'twitter_id',
    ),
);

$response = $api->get($data);
var_dump($response->getResult());

$api = new API\Form();
$data = array(
    'fuuid' => 'f4c74569-6cdf-4c44-a157-4990d4aa5b6d',
    'puuid' => $response->getResult()['response'],
    'data' => array(
        'c7' => 'first name',
        // 'c8' => 'last name',
    ),
);

var_dump($data);

$response = $api->store($data);
var_dump($response->getResult());