<?php
namespace SCRMHub\SDK\Core;

use SCRMHub\SDK\API;

class Response {
    private
        $headers,
        $postData,
        $rawData,
        $ch,
        $data;

    function __construct($headers, $postData, $rawData, $ch) {
        $this->headers      = $headers;
        $this->postData     = $postData;
        $this->rawData      = $rawData;
        $this->ch           = $ch;
        $this->decodeData($rawData);
    }

    //Decode response if needed
    private function decodeData($rawData) {
        $contentType = preg_split('|;|', $this->headers['content_type']);

        if($contentType[0] === 'application/json') {
            $this->data   = json_decode($rawData, true);
        } else {
            $this->data   = $rawData;
        }
    }

    function getData() {
        return $this->data;
    }

    function getResult() {
        return isset($this->data['result']) ? $this->data['result'] : false;
    }

    function isOk() {
        return isset($this->data['ok']) ? $this->data['ok']: false;
    }

    function getError() {
        $curlErrNo = curl_errno($this->ch);
        // ignore maximum redirects reached
        if ($curlErrNo > 0 && $curlErrNo != 47) {
            return curl_error($this->ch);
        } else {
            return isset($this->data['error']) ? $this->data['error'] : false;
        }
    }

    function getPostData() {
        return $this->postData;
    }

    function getHeaders($key = null) {
        if($key == null)
            return $this->headers;

        if(isset($this->headers[$key]))
            return $this->headers[$key];

        return false;
    }

    function getAll() {
        return array(
            'request'  => $this->request,
            'rawData'=> $this->rawData,
            'data'   => $this->data,
            'response' => $this->headers,
        );
    }
}