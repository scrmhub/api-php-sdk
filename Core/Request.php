<?php
namespace SCRMHub\SDK\Core;

use SCRMHub\SDK\API;
use SCRMHub\SDK\Core\Response;

class Request {
    private
        $api,
        $request,
        $headers,
        $opts,
        $response;

    private static
        $lastRequest = array(),
        $lastResponse;

    /**
     * Get the last request for debugging if required
     */
    public static function getLastRequest() {
        return self::$lastRequest;
    }

    /**
     * Get the last Response
     */
    public static function getLastResponse() {
        return self::$lastResponse;
    }

    public function setHeaders($headers) {
        $this->headers = $headers;
    }

    public function getHeaders() {
        return $this->headers;
    }

    /*
     * Save / send teh request
     */
    public function call($api, $request) {
        //Get the API options
        $options = API::getInstance()->getOptions();

        //Prepare the data
        $request = $this->prepareDataForSend($request);

        //Add in any base options
        $request = $this->addBaseOptions($request, $options['baseOptions']);

        //Create the options
        $opts = $options['options']['curlOptions'];

        //Add the URL
        $opts[CURLOPT_URL] = $options['options']['url'].$api.'/';

        //Starting adding to the request
        $opts[CURLOPT_POSTFIELDS]       = http_build_query($request);
        $opts[CURLOPT_CONNECTTIMEOUT]   = 10;

        $baseHeaders = $this->getHeaders();

        // disable the 'Expect: 100-continue' behaviour. This causes CURL to wait
        // for 2 seconds if the server does not support this header.
        $existingHeaders = isset($opts[CURLOPT_HTTPHEADER]) ? $opts[CURLOPT_HTTPHEADER] : array();
        $existingHeaders = array_merge($existingHeaders, $baseHeaders);
        $existingHeaders[] = 'Expect:';
        if ($existingHeaders['user-agent']) {
            $opts[CURLOPT_USERAGENT] = $existingHeaders['user-agent'];
        }

        $opts[CURLOPT_HTTPHEADER] = $existingHeaders;

        //Do we need a response?
        if(isset($request['returntransfer']) && $request['returntransfer'] === false) {
            $opts[CURLOPT_RETURNTRANSFER] = false;
        }

        //And send it
        $response = $this->sendRequest($opts);

        //Save everything for laters
        $this->api      = $api;
        $this->request  = $request;
        $this->opts     = $opts;
        $this->response = $response;

        //Return it
        return $response;
    }

    /*
     * Add any base options
     */
    private function addBaseOptions($request, $baseOptions) {
        //Add in any base options
        foreach($baseOptions as $key => $value) {
            if(!isset($request[$key]) || empty($request[$key]) || $request[$key] === null)
                $request[$key] = $value;
        }

        //Return it
        return $request;
    }

    /*
     * Send the request
     * Bad Token: {"ok":false,"result":{"code":500,"message":"Invalid Application Token"}}
     */
    private function sendRequest($opts) {
        //Get the API options
        $apiCore = API::getInstance();
        $options = $apiCore->getOptions();

        $ch = curl_init();

        //Set out options
        curl_setopt_array($ch, $opts);
        $result     = curl_exec($ch);

        //The response code
        $httpCode   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //Store for later
        self::$lastRequest = array(
            'opts'  => $opts,
            'curl'  => $ch
        );

        //Page not found
        if($httpCode == 404) {
            error_log('API Error 404: ' . print_r($opts, true));

        //Server error
        } else if($httpCode == 500) {
            error_log('API Error 500: ' . print_r($opts, true));
        }

        //The response object
        $response = new Response(curl_getinfo($ch), $opts[CURLOPT_POSTFIELDS], $result, $ch);
        $errorResponse = $response->getError();

        // response hooks
        if (!$response->isOk() && !empty($errorResponse)) {
            $errorCallback = $apiCore->getErrorCallback();
            if (!empty($errorCallback)) {
                $errorCallback($errorResponse);
            }
        }

        $errorHooks = $apiCore->getErrorHooks();
        if (is_array($errorHooks)) foreach ($errorHooks as $errorHook) {
            $errors = $errorHook['errors'];
            $callback = $errorHook['callback'];
            if (is_array($errors)) foreach ($errors as $error) {
                list($code, $subcode) = $error;

                if (($code == $httpCode) && ($subcode == $errorResponse['code'])) {
                    $callback();
                }
            }
        }

        //decode the response
        return $response;
    }


    /**
     * Prepare the data to be sent correctly
     */
    private function prepareDataForSend($data) {
        $params = array();

        //Loop through the fields
        foreach($data as $key => $value) {
            if($value === null)
                continue;

            $params[$key] = is_array($value) ? json_encode($value) : $value;
        }

        return $params;
    }
}