<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Stats extends Base {
    /**
     * Our variables
     */
    protected
        $api = 'stats',
        $baseRequest = array(
            'action' => null
        );
}