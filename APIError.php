<?php
namespace SCRMHub\SDK;

class APIError {
    // External errors
    public static $EXTERNAL_ERROR              = array(400, 400801);
    public static $EXTERNAL_CONNECT_CANCEL     = array(400, 400802);
    public static $EXTERNAL_MISCONFIGURED      = array(400, 400803);
    public static $EXTERNAL_AUTH               = array(400, 400804);
    
    // Puuid errors
    public static $NOT_FOUND_PERSON            = array(400, 400604);
    public static $NOT_FOUND_PERSON_IDENTIFIER = array(400, 400605);

    // Auth errors
    public static $AUTH_EXPIRED_APP_KEY        = array(401, 401101);
    public static $AUTH_EXPIRED_TOKEN          = array(401, 401102);
    public static $AUTH_INVALID_APP_KEY        = array(401, 401201);
    public static $AUTH_INVALID_APP_SECRET     = array(401, 401202);
    public static $AUTH_INVALID_OAUTH_TOKEN    = array(401, 401203);
    public static $AUTH_INVALID_TOKEN          = array(401, 401204);
    public static $AUTH_INVALID_TOKEN_DEVICE   = array(401, 401205);
    public static $AUTH_NOT_FOUND_USER         = array(401, 401301);
    public static $AUTH_PASSWORD_MISMATCH      = array(401, 401401);
    public static $AUTH_PASSWORD_INVALID       = array(401, 401402);

    public static function tokenErrors() {
        return array(
            static::$AUTH_EXPIRED_TOKEN,
            static::$AUTH_INVALID_TOKEN,
            static::$AUTH_INVALID_TOKEN_DEVICE,
        );
    }

    public static function puuidErrors() {
        return array(
            static::$NOT_FOUND_PERSON,
            static::$NOT_FOUND_PERSON_IDENTIFIER,
        );
    }
}