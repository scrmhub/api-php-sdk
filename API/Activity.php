<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\API;
use SCRMHub\SDK\Core\Base;
use SCRMHub\SDK\Core\Request;

class Activity extends Base {
    /**
     * Our variables
     */
    protected
        $api        = 'activity',
        $baseRequest = array(
            'action'        => null,
            'puuid'         => null,
            'location'      => null,
            'type'          => null,
            'useraction'    => null,
            'id'            => null,
            'target'        => null,
            'isfan'         => null,
            'refid'         => null,
            'referrer'      => null,
            'refdata'       => null,
            'agent'         => null,
            'ip'            => null,
            'response'      => true,
            'createpuuid'   => true
        );

    // hack to run api on old api url
    public function load($data) {
        $apiCore = API::getInstance();
        $oldUrl = $apiCore->getOptions()['options']['url'];
        $apiCore->setup(array(
            'url' => $apiCore->getOverrideUrl() ?: 'https://scrmhub.com/api/',
        ));


        $response = parent::load($data);

        $apiCore->setup(array('url' => $oldUrl));

        return $response;
    }

    /**
     * Track a page view
     */
    function trackView($url, $puuid = null, $fan = null) {
        //Put together the request
        $request = array(
            'action'        => 'create',
            'puuid'         => $puuid,
            'isfan'         => $fan,
            'type'          => 'page',
            'useraction'    => 'view',
            'id'            => $url
        );

        return $this->load($request);
    }

    function trackFormEntry($fuuid, $euuid) {
        //Put together the request
        $request = array(
            'action'        => 'create',
            'type'          => 'form',
            'useraction'    => 'entry',
            'target'        => $fuuid,
            'id'            => $euuid,
        );

        return $this->load($request);
    }

    /**
     * Track a Social Share
     */
    function trackSocial($network, $action = 'share', $shareUrl = null, $sourceUrl = null, $networkId = null, $puuid = null, $fan = null) {
        //Put together the request
        $request = array(
            'action'        => 'create',
            'puuid'         => $puuid,
            'isfan'         => $fan,
            'type'          => 'social',
            'target'        => $network,
            'useraction'    => $action,
            'id'            => $shareUrl,
            'referrer'      => $sourceUrl,
            'refdata'       => $networkId,
            'response'      => false
        );

        return $this->load($request);
    }

    /**
     * Track a Social Share
     */
    function trackEvent($target, $action = null, $identifier = null, $sourceUrl = null, $refData = null, $puuid = null, $fan = null) {
        //Put together the request
        $request = array(
            'action'        => 'create',
            'type'          => 'event',
            'puuid'         => $puuid,
            'isfan'         => $fan,
            'useraction'    => $action,
            'target'        => $target,
            'id'            => $identifier,
            'referrer'      => $sourceUrl,
            'response'      => false
        );

        return $this->load($request);
    }

    /**
     * Track a download
     */
    function trackDownload($url, $extension = null, $puuid = null, $fan = null) {
        //Put together the request
        $request = array(
            'action'        => 'create',
            'puuid'         => $puuid,
            'isfan'         => $fan,
            'type'          => 'file',
            'useraction'    => 'download',
            'target'        => $extension,
            'id'            => $url
        );

        return $this->load($request);
    }

    /**
     * Track an Outbound link
     */
    function trackOutbound($url, $sourceUrl = null, $puuid = null, $fan = null) {
        //Put together the request
        $request = array(
            'action'        => 'create',
            'puuid'         => $puuid,
            'isfan'         => $fan,
            'type'          => 'page',
            'useraction'    => 'outbound',
            'id'            => $url,
            'referrer'      => $sourceUrl
        );

        return $this->load($request);
    }

    /**
     * Return the base request data
     * This can be overriden by the api class if required
     */
    protected function getBaseRequest($data) {
        //Add an IP
        if(!isset($data['ip']) || $data['ip'] == '')
            $this->baseRequest['ip'] = $this->getRealIp();

        //Add an agent
        if(!isset($data['agent']) || $data['agent'] == '')
            $this->baseRequest['agent'] = $_SERVER['HTTP_USER_AGENT'];

        //Add a referrer if available
        if((!isset($data['referrer']) || $data['referrer'] == '') && isset($_SERVER['HTTP_REFERER']))
            $this->baseRequest['referrer'] = $_SERVER['HTTP_REFERER'];

        //Return the object
        return $this->baseRequest;
    }

    /**
     * Save an activity
     *
     * Return an id for the activity
     */
    private function getRealIp() {
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            //check for ip from share internet
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            // Check for the Proxy User
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            //The usual
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }
}