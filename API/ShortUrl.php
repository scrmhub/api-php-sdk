<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\API;
use SCRMHub\SDK\Core\Base;
use SCRMHub\SDK\Core\Request;

class ShortUrl extends Base {
        /**
         * Our variables
         */
        protected
                $api = 'url',
                $baseRequest = array(
                        'action'        => null,
                        'puuid'         => null,
                        'location'      => null,
                        'url'           => null,
                        'url_data'  => null,
                );

    public function getApiUrl() {
        $apiCore = API::getInstance();

        $url  = $apiCore->getOptions()['options']['url'];
        $url .= $this->api . '/';

        return $url;
    }
}