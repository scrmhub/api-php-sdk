<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;
use SCRMHub\SDK\Core\Request;

class Person extends Base {
        /**
         * Our variables
         */
        protected
                $api = 'person',
                $baseRequest = array(
                        'action'        => null,
                        'puuid'         => null,
                        'usertoken'     => null,
                        'ids'           => null,
                        'data'          => null,
                        'pw'            => null
                );
}