<?php
namespace SCRMHub\SDK;

use SCRMHub\SDK\APIError;

class API {
    //Our class holder
    private static
        $instance,
        $batch = false;

    //Default options
    private
        $overrideUrl,
        $options = array(
            'url'           => 'https://api.scrmhub.com/',
            'curlOptions'   => array(
                CURLOPT_CONNECTTIMEOUT  => 3,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_TIMEOUT         => 60,
                CURLOPT_USERAGENT       => 'sogial-goo-0.1',
                CURLOPT_FOLLOWLOCATION  => true,
                CURLOPT_HEADER          => false,
                CURLOPT_POST            => true,
                CURLOPT_SSL_VERIFYPEER  => false, //Disable ssl check
                CURLOPT_RETURNTRANSFER  => true,
            ),
        ),
        $reservedKeys = array(
            'appkey',
            'action',
            'refdata'
        ),
        $baseOptions = array(),
        $errorCallback = null,
        $errorHooks = array();


    /**
     * Get the API instance
     */
    static function getInstance() {
        if(!self::$instance)
            self::$instance = new static();

        return self::$instance;
    }

    /**
     * Initialise the class
     */
    static function init($baseOptions = array()) {
        $instance = self::getInstance();
        $instance->setup($baseOptions);

        return $instance;
    }

    static function onError($callback) {
        $instance = self::getInstance();
        $instance->errorCallback = $callback;
    }

    static function onErrorCodes($errors, $callback) {
        $instance = self::getInstance();
        $instance->errorHooks[] = array(
            'errors' => $errors,
            'callback' => $callback,
        );
    }

    public function getErrorCallback() {
        return $this->errorCallback;
    }

    public function getErrorHooks() {
        return $this->errorHooks;
    }

    static function onTokenError($callback) {
        $errors = APIError::tokenErrors();
        static::onErrorCodes($errors, $callback);
    }

    static function onPuuidError($callback) {
        $errors = APIError::puuidErrors();
        static::onErrorCodes($errors, $callback);
    }

    /**
     * Set options
     */
    public function setup($options = array()) {
        if(!empty($options)) {
            $this->options = array_merge($this->options, $options);
        }

        foreach ($this->options as $key => $val) {
            if (in_array($key, $this->reservedKeys))
                $this->baseOptions[$key] = $val;
        }
    }

    /**
     * Set a value
     */
    public function set($key, $value) {
        if(!in_array($key, $this->reservedKeys))
            $this->baseOptions[$key] = $value;
    }

    /**
     * Get a value
     */
    public function get($key) {
        if(isset($this->baseOptions[$key]))
            return $this->baseOptions[$key];

        return false;
    }

    /**
     * Return the configuration options
     */
    public function getOptions() {
        $options = array(
            'options'       => $this->options,
            'baseOptions'   => $this->baseOptions
        );

        return $options;
    }

    /**
     * Start a batch of requests
     */
    public static function startBatch() {
        //Start Batch
        //Make Batch an array object
        self::$batch = new Core\Batch();
    }

    /**
     * check if we're in the middle of a batch or not
     */
    public static function isBatch() {
        return self::$batch !== false;
    }

    /**
     * Add a call to a batch
     */
    public static function addToBatch($api, $opts) {
        self::$batch->add($api, $opts);
    }

    /**
     * Send a batch
     */
    public static function sendBatch() {
        //Get the response
        $response = self::$batch->call();

        //Stop batching
        self::$batch = false;

        //Return the response
        return $response;
    }

    public function setOverrideUrl($url) {
        $this->overrideUrl = $url;
    }

    public function getOverrideUrl() {
        return $this->overrideUrl;
    }
}