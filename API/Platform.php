<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;
use SCRMHub\SDK\API;

class Platform extends Base {
    /**
     * Our variables
     */
    protected
        $api = 'platform',
        $baseRequest = array(
            'action'        => null,
            'puuid'         => null
        );

    public function connect($data) {
        $apiCore = API::getInstance();
        $oldOptions = $apiCore->getOptions()['options']['curlOptions'];
        $newOptions = array(
            CURLOPT_MAXREDIRS => 1,
            // CURLOPT_FOLLOWLOCATION => 0
        );

        $apiCore->setup(array(
            'curlOptions' => $oldOptions + $newOptions
        ));

        $response = parent::connect($data);
        $apiCore->setup(array('curlOptions' => $oldOptions));

        return $response;
    }
}