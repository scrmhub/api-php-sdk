<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Game extends Base {
    /**
     * Our variables
     */
    protected
        $api = 'game',
        $baseRequest = array(
            'action'        => null,
            'puuid'         => null
        );
}