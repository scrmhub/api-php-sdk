<?php
namespace SCRMHub\SDK\API;

use SCRMHub\SDK\Core\Base;

class Question extends Base {
    /**
     * Our variables
     */
    protected
        $api = 'question',
        $baseRequest = array(
            'action'        => null,
            'puuid'         => null,
            'questions'     => null,
            'questionkeys'  => null
        );
}